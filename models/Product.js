const mongoose = require('mongoose');


const productSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true,
    },
    creationDate: {
        type: Date,
        required: true, 
        default: Date.now
    },
    stock: {
        type: Number,
        required: true,
        //min: [1, 'Quantity can not be less then 1.']
    },
    cartQuantity: {
        type: Number,
        default: 0
    },
    bestSeller: {
        type: Boolean,
        default: false
    }
});


module.exports = mongoose.model('Product', productSchema);