const mongoose = require('mongoose');
const Product = require('./Product')


const cartSchema = new mongoose.Schema({
    products: [Product.schema],
    createdAt: {
        type: Date,
        default: Date.now
    }
});


module.exports = mongoose.model('Cart', cartSchema);