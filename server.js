require('dotenv').config()

const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');


//DB CONNECTION
mongoose.connect(process.env.DATABASE_URL, { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection
db.on('error', (error) => console.log(error))
db.once('open', () => console.log('Connected to DB successfully!'))


//MIDDLEWARES
app.use(express.json());
app.use(express.urlencoded({ extended: true }))
app.use(bodyParser.json());


//IMPORT ROUTES
const productsRouter = require('./routes/products');
app.use('/products', productsRouter);

const cartRouter = require('./routes/cart');
app.use('/carts', cartRouter);

const cartItemsRouter = require('./routes/cartItems');
app.use('/', cartItemsRouter);


//PORT LISTEN
app.listen(3000, () => console.log('Server Started!'));