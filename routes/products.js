const express = require('express');
const router = express.Router();
const Product = require('../models/Product');


//GET ALL PRODUCTS
router.get('/', async (req, res) => {
    try {
        const products = await Product.find();
        res.json(products);
    } catch (error) {
        res.status(500).json({ message: error});
    }
});


//GET ALL PRODUCTS "BEST SELLER" 
router.get('/best', async (req, res) => {
    const products = await Product.find({bestSeller: true});
    try {
        return res.json(products);
    } catch (error) {
        res.status(500).json({ message: 'Theres no products best seller.'});
    }
});


//GET SPECIFIC PRODUCT
router.get('/:id', getProduct, (req, res) => {
    res.json(res.product);
});


//CREATE PRODUCT
router.post('/', async (req, res) => {
    const product = new Product({
        title: req.body.title,
        description: req.body.description,
        price: req.body.price,
        stock: req.body.stock
    })

    try {
        const newProduct = await product.save()
        res.status(201).json(newProduct);
    } catch (error) {
        res.status(400).json({ message: error });
    }
});


//UPDATE SPECIFIC PRODUCT
router.put('/:id', getProduct, async (req, res) => {
    if(req.body.title != null) {
        res.product.title = req.body.title
    }
    if(req.body.description != null) {
        res.product.description = req.body.description
    }
    if(req.body.price != null) {
        res.product.price = req.body.price
    }
    if(req.body.stock != null) {
        res.product.stock = req.body.stock
    }
    if(req.body.bestSeller != null) {
        res.product.bestSeller = req.body.bestSeller
    }
    try {
        const updatedProduct = await res.product.save()
        res.json(updatedProduct);
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});


//DELETE A PRODUCT
router.delete('/:id', getProduct, async (req, res) => {
    try {
        await res.product.remove()
        res.json({ message: 'Deleted Product' });
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
});


//MIDDLEWARE FUNCTION
async function getProduct(req, res, next) {
    let product
    try {
        product = await Product.findById(req.params.id)
        if(product == null) {
            return res.status(404).json({ message: 'Cannot find product' });
        }
    } catch (error) {
        return res.status(500).json({ message: error.message });
    }

    res.product = product
    next()
}



module.exports = router;