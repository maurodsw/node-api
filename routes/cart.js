const express = require('express');
const router = express.Router();
const Cart = require('../models/Cart');


//GET ALL CARTS
router.get('/', async (req, res) => {
    try {
        const carts = await Cart.find();
        res.json(carts);
    } catch (error) {
        res.status(500).json({ message: error});
    }
});


//GET SPECIFIC CART
router.get('/:id', getCart, (req, res) => {
    res.json(res.cart);
});


//CREATE CART
router.post('/', async (req, res) => {
    const cart = new Cart({
        products: req.body.products,
    })

    try {
        const newCart = await cart.save()
        res.status(201).json(newCart);
    } catch (error) {
        res.status(400).json({ message: error });
    }
});


//DELETE A CART
router.delete('/:id', getCart, async (req, res) => {
    try {
        await res.cart.remove()
        res.json({ message: 'Deleted Cart' });
    } catch (error) {
        res.status(500).json({ message: error.message })
    }
});


//MIDDLEWARE FUNCTION
async function getCart(req, res, next) {
    let cart
    try {
        cart = await Cart.findById(req.params.id)
        if(cart == null) {
            return res.status(404).json({ message: 'Cannot find cart' });
        }
    } catch (error) {
        return res.status(500).json({ message: error.message });
    }

    res.cart = cart
    next()
}



module.exports = router;