const express = require('express');
const router = express.Router();
const Cart = require('../models/Cart');
const Product = require('../models/Product');


//ADD PRODUCT TO A CART
router.post('/cart/:cartId/product/:productId' , async (req, res) => {
    //Check if cart exits
    if (req.params.cartId !== 'null') {
        
          Cart.findById(req.params.cartId, (err, foundCart) => {
          if (err) return res.status(500).json({ status: 500, error: 'Something went wrong. Please try again' });
    
          Product.findById(req.params.productId, (err, foundProduct) => {
          if (err) return res.status(500).json({ status: 500, error: 'Something went wrong. Please try again' });
    
          const cartProduct = foundCart.products.id(foundProduct._id);
    
          // If the item already exists in the cart, update the product stock and cart quantity
          if (cartProduct) {

            cartProduct.cartQuantity++;
            cartProduct.stock--;

            if(cartProduct.stock == 0){
              return res.status(500).json({ status: 500, error: 'This item is out of stock' });
            }
            
    
            foundCart.save((err, savedCart) => {
              if (err) return res.status(500).json({ status: 500, error: 'Something went wrong. Please try again' });
    
              res.status(201).json({
                status: 201,
                data: { cart: savedCart },
                dateRequested: new Date().toLocaleString(),
              });
            });
              
          } else {
            // If the item does not already exist in the cart, add it
            // and only edit the item stock in the cart
    
            foundProduct.stock--;
            foundProduct.cartQuantity++;

            foundCart.products.push(foundProduct);
      
            foundCart.save((err, savedCart) => {
              if (err) return res.status(500).json({ status: 500, error: 'Something went wrong. Please try again' });
              res.status(201).json({
                status: 201,  
                data: { cart: savedCart },
                dateRequested: new Date().toLocaleString(),
              });
            });
        }
      });
    });

    } else {
        // Create cart if one does not already exist
        // This function is not working yet
        Product.findById(req.params.productId, (err, foundProduct) => {
        if (err) return res.status(500).json({ status: 500, error: 'Something went wrong. Please try again' });
            
    
        // Create New Cart
        const newCart = new Cart();
        newCart.products.push(foundProduct);
        newCart.save((err, savedCart) => {
          if (err) return res.status(500).json({ status: 500, error: 'Something went wrong. Please try again' });
    
          res.status(201).json({
            status: 201,
            data: { cart: savedCart, product: foundProduct },
            dateRequested: new Date().toLocaleString(),
          });
        });
    
      });
    }
});


//REMOVE PRODUCT FROM SPECIFIC CART
router.put('/cart/:cartId/product/:productId/remove' , async (req, res) => {
  Cart.findById(req.params.cartId, (err, foundCart) => {
    if (err) return res.status(500).json({ status: 500, error: 'Something went wrong. Please try again' });

    const cartProduct = foundCart.products.id(req.params.productId);

    if (cartProduct) {
      cartProduct.remove();
    }

    foundCart.save((err, savedCart) => {
      if (err) return res.status(500).json({ status: 500, error: 'Something went wrong. Please try again' });

      res.status(200).json({
        status: 200,
        data: cartProduct,
        dateRequested: new Date().toLocaleString(),
      });
    });
  });
});


module.exports = router;